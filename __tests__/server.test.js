const supertest = require('supertest');
const logger = require('simple-node-logger').createSimpleLogger();
const { Server } = require('../lib/server');


jest.mock('../lib/handler', () => ({
  getVersion: jest.fn(() => Promise.resolve()),
}));


describe('server', () => {
  let server;
  describe('handles response', () => {
    // let response;
    beforeEach(() => {
      server = new Server(logger);
    });
  });

  describe('handles request', () => {
    describe('success', () => {
      let response;
      describe('health', () => {
        beforeEach(async (done) => {
          server = new Server(logger);
          response = await supertest(server.app)
            .get('/health');
          done();
        });
        it('receives 200', () => {
          expect(response.statusCode).toEqual(200);
        });
      });
      describe('version', () => {
        beforeEach(async (done) => {
          server = new Server(logger);
          response = await supertest(server.app)
            .get('/version');
          done();
        });
        it('receives 200', () => {
          expect(response.statusCode).toEqual(200);
        });
      });
    });
  });
});
