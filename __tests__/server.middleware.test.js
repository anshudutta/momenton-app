const express = require('express');
const bodyParser = require('body-parser');
const logger = require('simple-node-logger').createSimpleLogger();
const { Server } = require('../lib/server');

const port = 3000;

const app = {
  use: jest.fn(),
  all: jest.fn(),
  get: jest.fn(() => port),
  set: jest.fn(),
  address: jest.fn(),
  options: jest.fn(),
  listen: jest.fn(() => ({ foo: 'bar' })),
  post: jest.fn(),
  put: jest.fn(),
  delete: jest.fn(),
};
jest.mock('express');
express.mockImplementation(() => app);

describe('Server', () => {
  let server;

  describe('setup', () => {
    beforeEach(() => {
      server = new Server(logger);
    });
    describe('app initialised with middleware', () => {
      it('sets body-parser middleware', () => {
        expect(JSON.stringify(app.use.mock.calls[0][0])).toEqual(
          JSON.stringify(bodyParser.json()),
        );
      });
      it('sets body-parser urlencoded', () => {
        expect(JSON.stringify(app.use.mock.calls[1][0])).toEqual(
          JSON.stringify(
            bodyParser.urlencoded({
              extended: true,
            }),
          ),
        );
      });
    });
    describe('start app', () => {
      beforeEach(() => {
        server.start(port);
      });
      it('listens on port', () => {
        expect(app.listen).toHaveBeenCalledWith(port);
      });
    });
  });
});
