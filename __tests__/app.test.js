const { start } = require('../app');
const { Server } = require('../lib/server');

const app = {
  start: jest.fn(),
};
jest.mock('../lib/server');
Server.mockImplementation(() => app);

jest.mock('../lib/server');

describe('app', () => {
  beforeEach(() => {
    start(8080);
  });
  describe('start', () => {
    it('inits the main server', () => {
      expect(app.start).toHaveBeenCalledWith(8080);
    });
  });
});
