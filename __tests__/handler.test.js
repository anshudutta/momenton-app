const { getVersion } = require('../lib/handler');
const pjson = require('../package.json');

jest.mock('config', () => ({
  versionDefinition: {
    lastCommitSha: 'some hash',
    description: 'something awesome',
  },
}));

describe('handler', () => {
  let appVersion;

  describe('getVersion', () => {
    beforeEach(() => {
      appVersion = getVersion();
    });
    it('returns version', () => {
      expect(appVersion).toEqual({
        version: pjson.version,
        lastCommitSha: 'some hash',
        description: 'something awesome',
      });
    });
  });
});
