const supertest = require('supertest');
const logger = require('simple-node-logger').createSimpleLogger();
const { Server } = require('../lib/server');

jest.mock('../lib/handler', () => ({
  getVersion: jest.fn(() => {
    throw new Error('covid-19');
  }),
}));


describe('server', () => {
  let server;
  describe('handles response', () => {
    beforeEach(() => {
      server = new Server(logger);
    });
  });

  describe('handles request', () => {
    describe('failure', () => {
      let response;
      describe('404', () => {
        beforeEach(async (done) => {
          server = new Server(logger);
          response = await supertest(server.app).get('/badass');
          done();
        });
        it('returns status code 404', () => {
          expect(response.status).toEqual(404);
        });
      });
      describe('500', () => {
        beforeEach(async (done) => {
          server = new Server(logger);
          response = await supertest(server.app).get('/version');
          done();
        });
        it('returns status code 500', () => {
          expect(response.status).toEqual(500);
        });
      });
    });
  });
});
