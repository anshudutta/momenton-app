# Version-App

App for reflecting api version.

## Installation

### Test
```
$ npm install
$ npm test
```

### Build
```
$ docker build . \
--build-arg LAST_COMMIT_SHA="last-hash-commit" \
-t version-app:latest
```

### Run
Pass the port where you want it to listen
```
$docker run --rm -d --name version-api \
-e PORT=8080 -p 8080:8080 \
version-app:latest
```

## Usage

```
curl localhost:8080/version
```

## Version
Handled manually for this example
```
$ npm run version:patch
$ npm run version:minor
$ npm run version:major
```
## CI
Pipeline is setup in Gitlab repo

https://gitlab.com/anshudutta/momenton-app/-/pipelines

Images are uploaded @ registry.gitlab.com/anshudutta/momenton-app

```
Lint -> Test -> Audit Packages -> Docker Build -> Upload Image -> Auto Deploy (Not setup)
```

## K8 Deployment
A helm chart is available at `/charts/momenton-app`

Tested on minikube, but should work on any cluster.
1. Get cluster connection
2. Create a namespace `technical-test`
```
$ kubectl create namespace technical-test
```
3. Install chart
```
$ cd charts
$ helm install -n technical-test momenton-app ./momenton-app
$ helm -n technical-test status momenton-app
```
4. Access the service
```
$ kubectl -n technical-test port-forward svc/momenton-app 8080:8080
$ curl localhost:8080/version
{"version":"1.0.0","lastCommitSha":"a1407f9d","description":"Version Information"}
```
## Limitations
This is not a production setup but can be extended to be one
1. Images are publicly accessible and deployment not setup with `imagePullSecerts`
2. Service is `ClusterIP` and accessible via port-forward. Ingress is not setup
3. CI doesn't bump package version automatically
4. No security is implemented in the service
5. Git hooks not setup to run on local commit

## License
[MIT](https://choosealicense.com/licenses/mit/)