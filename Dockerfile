FROM node:14-alpine

ARG LAST_COMMIT_SHA
ENV LAST_COMMIT_SHA $LAST_COMMIT_SHA
ENV NODE_ENV production

WORKDIR /srv

COPY package.json .
COPY package-lock.json .

RUN npm install

COPY . ./

CMD [ "node", "app.js" ]
