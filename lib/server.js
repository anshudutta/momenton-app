const express = require('express');
const bodyParser = require('body-parser');
const { register } = require('./routes');

/* eslint no-underscore-dangle: ["error", { "enforceInMethodNames": true }] */
/* eslint no-underscore-dangle: ["error", { "allowAfterThis": true  }] */
/* eslint no-unused-vars: "off" */

class Server {
  constructor(logger) {
    this._logger = logger;
    this._app = express();

    this._app.use(bodyParser.json());
    this._app.use(
      bodyParser.urlencoded({
        extended: true,
      }),
    );
    register(this._app);

    // 404 handler
    this._app.use((_req, res) => {
      const err = new Error("That route doesn't exist");
      err.statusCode = 404;
      res.status(404).send(`Not found: ${err.message}`);
    });

    // Error handler
    this._app.use((err, _req, res, _next) => {
      this._logger.error(err);
      res.status(500).send(`Internal server error: ${err.message}`);
    });
  }

  get app() {
    return this._app;
  }

  start(port) {
    this._app.set('port', port);
    this._server = this._app.listen(this._app.get('port'));
    this._logger.info({ port }, 'Listening');
  }
}

module.exports = {
  Server,
};
