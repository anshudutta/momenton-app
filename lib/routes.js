const {
  getVersion,
} = require('./handler');

const register = (app) => {
  app.get('/health', (_req, res) => {
    res.status(200).json({ status: 'ok' });
  });
  app.get('/version', (_req, res, next) => {
    try {
      res.status(200).json(getVersion());
    } catch (err) {
      next(err);
    }
  });
};

module.exports = {
  register,
};
