const config = require('config');
const pjson = require('../package.json');

const { versionDefinition } = config;

const getVersion = () => ({
  version: pjson.version,
  lastCommitSha: versionDefinition.lastCommitSha,
  description: versionDefinition.description,
});

module.exports = {
  getVersion,
};
