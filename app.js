const logger = require('simple-node-logger').createSimpleLogger();
const config = require('config');
const { Server } = require('./lib/server');

const start = (port) => {
  const server = new Server(logger);
  server.start(port);
};

start(config.port);

module.exports = {
  start,
};
